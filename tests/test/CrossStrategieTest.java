/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import bataillenavale.game.Coordonnees;
import bataillenavale.game.EtatPartie;
import bataillenavale.game.Grille;
import bataillenavale.game.OptionsJeu;
import bataillenavale.game.ia.CrossStrategie;
import bataillenavale.game.ia.StrategieIAFactory;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Marie
 */
public class CrossStrategieTest {
	@Test
	public void testJouer() throws Exception {
		EtatPartie etat = new EtatPartie(new OptionsJeu("cross", 10, 10, "moderne", "random"));
		Grille joueur = etat.getGrilleJoueur(), ia = etat.getGrilleIA();


		System.out.println("Tirs :");
		joueur.printContent();
		System.out.println("Navires : ");
		ia.printContent();
		System.out.println("Portee :");
		ia.printPortee();

		CrossStrategie strategie = (CrossStrategie) StrategieIAFactory.getStrategie("cross");
		try {
			Coordonnees coord;
			do {
				coord = strategie.jouer(etat);
				System.out.print(coord + " ");
				joueur.tir(coord);
				etat.verifieFinJeu();
			} while (!etat.isFini() && coord != null);
			assertTrue("Etat de fin non atteint", etat.isFini()); // Reussi si on arrive a un etat de fin
			assertNotNull("Null renvoyé avant la fin de la boucle", coord); // Reussi si l'ia n'a jamais renvoye null
			System.out.println();
			joueur.printContent();
		} catch (IllegalArgumentException e) {
			assertFalse(true); // Test échoué
		}
	}
}
