package test;

import bataillenavale.game.Coordonnees;
import bataillenavale.game.Grille;
import bataillenavale.game.Grille.GrilleNavire;
import bataillenavale.game.Navire;
import bataillenavale.game.epoque.EpoqueFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class GrilleTest {
	private Grille grille;
	private List<GrilleNavire> navs;

	@Before
	public void setup() throws Exception {
		grille = new Grille(10, 10, EpoqueFactory.getEpoque("moderne"));
		grille.addNavire(Navire.TINY, new Coordonnees(1, 1, 0));
		grille.addNavire(Navire.SMALL, new Coordonnees(3, 2, 2));
		grille.addNavire(Navire.LARGE, new Coordonnees(1, 3, 1));
		grille.addNavire(Navire.HUGE, new Coordonnees(5, 7, 3));
		navs = grille.getNavires();
	}

	@Test
	public void testAddNavire() throws Exception { // Note : canAdd est egalement testee
		grille.printContent();
		assertEquals(4, navs.size());
		GrilleNavire nav = navs.get(0);
		assertEquals(Navire.TINY, nav.getNavire());
		assertEquals(new Coordonnees(1, 1, 0), nav.getPosition());
		assertEquals(nav.getNavire().getTaille(), nav.getVie());

		nav = navs.get(1);
		assertEquals(Navire.SMALL, nav.getNavire());
		assertEquals(new Coordonnees(3, 2, 2), nav.getPosition());
		assertEquals(nav.getNavire().getTaille(), nav.getVie());

		nav = navs.get(2);
		assertEquals(Navire.LARGE, nav.getNavire());
		assertEquals(new Coordonnees(1, 3, 1), nav.getPosition());
		assertEquals(nav.getNavire().getTaille(), nav.getVie());

		nav = navs.get(3);
		assertEquals(Navire.HUGE, nav.getNavire());
		assertEquals(new Coordonnees(5, 7, 3), nav.getPosition());
		assertEquals(nav.getNavire().getTaille(), nav.getVie());

		assertFalse(grille.canAdd(Navire.TINY, new Coordonnees(2, 1, 2)));
		assertFalse(grille.canAdd(Navire.SMALL, new Coordonnees(3, 1, 1)));
		assertFalse(grille.canAdd(Navire.LARGE, new Coordonnees(0, 0, 2)));
		assertFalse(grille.canAdd(Navire.HUGE, new Coordonnees(0, 5, 0)));
	}

	@Test
	public void testGetNavire() throws Exception {
		assertEquals(navs.get(0), grille.getNavire(new Coordonnees(1, 1, 0)));
		assertEquals(navs.get(0), grille.getNavire(new Coordonnees(2, 1, 0)));

		assertEquals(navs.get(1), grille.getNavire(new Coordonnees(3, 2, 2)));
		assertEquals(navs.get(1), grille.getNavire(new Coordonnees(2, 2, 2)));
		assertEquals(navs.get(1), grille.getNavire(new Coordonnees(1, 2, 2)));

		assertEquals(navs.get(2), grille.getNavire(new Coordonnees(1, 3, 1)));
		assertEquals(navs.get(2), grille.getNavire(new Coordonnees(1, 4, 1)));
		assertEquals(navs.get(2), grille.getNavire(new Coordonnees(1, 5, 1)));
		assertEquals(navs.get(2), grille.getNavire(new Coordonnees(1, 6, 1)));

		assertEquals(navs.get(3), grille.getNavire(new Coordonnees(5, 7, 3)));
		assertEquals(navs.get(3), grille.getNavire(new Coordonnees(5, 6, 3)));
		assertEquals(navs.get(3), grille.getNavire(new Coordonnees(5, 5, 3)));
		assertEquals(navs.get(3), grille.getNavire(new Coordonnees(5, 4, 3)));
		assertEquals(navs.get(3), grille.getNavire(new Coordonnees(5, 3, 3)));
	}

	@Test
	public void testTir() throws Exception { // Et getTir
		grille.tir(new Coordonnees(1, 1));
		grille.tir(new Coordonnees(2, 1));
		grille.tir(new Coordonnees(5, 1));
		grille.tir(new Coordonnees(1, 4));
		grille.tir(new Coordonnees(2, 4));
		grille.tir(new Coordonnees(5, 4));
		grille.printContent();

		assertTrue(navs.get(0).isCoule());
		assertFalse(navs.get(1).isCoule());
		assertFalse(navs.get(2).isCoule());
		assertFalse(navs.get(3).isCoule());

		assertEquals(Navire.SMALL.getTaille(), navs.get(1).getVie());
		assertEquals(Navire.LARGE.getTaille() - 1, navs.get(2).getVie());
		assertEquals(Navire.HUGE.getTaille() - 1, navs.get(3).getVie());

		assertTrue(grille.getTir(new Coordonnees(1, 1)));
		assertTrue(grille.getTir(new Coordonnees(2, 1)));
		assertTrue(grille.getTir(new Coordonnees(5, 1)));
		assertTrue(grille.getTir(new Coordonnees(1, 4)));
		assertTrue(grille.getTir(new Coordonnees(2, 4)));
		assertTrue(grille.getTir(new Coordonnees(5, 4)));
	}
}
