package bataillenavale;

import bataillenavale.game.epoque.EpoqueModerne;
import bataillenavale.game.epoque.Renaissance;
import bataillenavale.game.ia.CrossStrategie;
import bataillenavale.game.ia.RandomStrategie;
import bataillenavale.vues.AssetManager;
import bataillenavale.vues.FenetrePrincipale;

import java.io.IOException;

public class Main {
	public static void main(String[] args) {
		try {
			AssetManager.getInstance().init();
			Renaissance.register();
			EpoqueModerne.register();
			RandomStrategie.register();
			CrossStrategie.register();
			new FenetrePrincipale(); // Creer la vue principale
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
