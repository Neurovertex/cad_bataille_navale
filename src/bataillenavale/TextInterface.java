package bataillenavale;

import bataillenavale.game.Coordonnees;
import bataillenavale.game.EtatPartie;
import bataillenavale.game.Grille;
import bataillenavale.game.Navire;

import java.util.InputMismatchException;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

/**
 * Cette classe represente une interface textuelle pour jouer au jeu par la console. Les commandes sont "tirer", "placer", "carte" et "carte totale" (revele les navires de l'IA)
 * @author Jeremie
 *         Date: 08/04/14, 20:57
 */
public class TextInterface extends Thread implements Observer {
	private EtatPartie etat;

	public TextInterface(EtatPartie etat) {
		setEtat(etat);
	}

	@Override
	public void run() {
		Scanner in = new Scanner(System.in);
		String cmd = "";
		do try {
			cmd = in.nextLine();
			if (cmd.equalsIgnoreCase("tirer")) {
				int x, y;
				System.out.println("X ?");
				x = in.nextInt();
				System.out.println("Y ?");
				y = in.nextInt();
				if (!etat.jouer(new Coordonnees(x, y)))
					System.err.println("Tir refusé");
			} else if (cmd.equalsIgnoreCase("placer")) {
				System.out.println("Type ?");
				Navire nav = Navire.valueOf(in.nextLine().trim().toUpperCase());
				if (nav == null)
					System.err.println("Navire inconnu");
				else {
					int x, y, r;
					System.out.println("X ?");
					x = in.nextInt();
					System.out.println("Y ?");
					y = in.nextInt();
					System.out.println("Rotation ? (0-3)");
					r = in.nextInt();
					if (!etat.getGrilleJoueur().canAdd(nav, new Coordonnees(x, y, r)))
						System.out.println("Impossible d'ajouter le bateau a cet endroit");
					else
						etat.getGrilleJoueur().addNavire(nav, new Coordonnees(x, y, r));
				}
			} else if (cmd.equalsIgnoreCase("carte")) {
				System.out.println("Grille joueur : ");
				etat.getGrilleJoueur().printContent(false);
				System.out.println("Grille IA : ");
				etat.getGrilleIA().printContent(true);
			} else if (cmd.equalsIgnoreCase("carte totale"))
				etat.getGrilleIA().printContent(false);
			else if (cmd.equalsIgnoreCase("portee"))
				etat.getGrilleJoueur().printPortee();
		} catch (InputMismatchException e) {
			System.err.println("Erreur dans la lecture d'une valeur. Essayez a nouveau.");
		} catch (IllegalArgumentException e) {
			System.err.println("Valeur non valide. Essayez a nouveau");
		} while (!cmd.equalsIgnoreCase("exit"));
	}

	public void setEtat(EtatPartie etat) {
		if (this.etat != null)
			etat.deleteObserver(this);
		this.etat = etat;
		etat.addObserver(this);
		etat.getGrilleIA().addObserver(this);
		etat.getGrilleJoueur().addObserver(this);
	}

	@Override
	public void update(Observable o, Object arg) {
		if (o == etat && arg instanceof EtatPartie.Etat) {
			EtatPartie.Etat state = (EtatPartie.Etat)arg;
			System.out.println("Nouvel etat : "+ state);
		} else if (o == etat.getGrilleJoueur()) {
			if (arg instanceof Grille.GrilleNavire) {
				System.out.println("Navire Joueur placé : "+ arg);
			} else if (arg instanceof Coordonnees) {
				System.out.println("Tir IA  en " + arg + " : " + (etat.getGrilleJoueur().getNavire((Coordonnees) arg) != null ? etat.getGrilleJoueur().getNavire((Coordonnees) arg).getNavire() + " touché" : "Dans l'eau"));
			}
		} else if (o == etat.getGrilleIA()) {
			if (arg instanceof Grille.GrilleNavire) {
				System.out.println("Navire IA placé : "+ arg);
			} else if (arg instanceof Coordonnees) {
				System.out.println("Tir Joueur en " + arg + " : " + (etat.getGrilleIA().getNavire((Coordonnees) arg) != null ? "Navire touché" : "Dans l'eau"));
			}
		}
	}
}
