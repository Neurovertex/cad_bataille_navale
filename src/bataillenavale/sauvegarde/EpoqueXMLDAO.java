/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale.sauvegarde;

import bataillenavale.game.Epoque;
import bataillenavale.game.epoque.EpoqueModerne;
import bataillenavale.game.epoque.Renaissance;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Marie
 */
public class EpoqueXMLDAO implements EpoqueDAO {
    
    private static EpoqueXMLDAO instance  = null;

    private EpoqueXMLDAO() { }

    public static EpoqueXMLDAO getInstance() {
        if (instance == null) {
            instance = new EpoqueXMLDAO();
        }
        return instance;
    }

    @Override
    public String load() {
        BufferedReader br = null;
        String line ;
        String epo = "" ;
        try {
            br = new BufferedReader(new FileReader("sauvegarde.xml"));
            while ((line = br.readLine()) != null) {
                if (line.startsWith("<Epoque>")) {
                    epo = line.replaceFirst("<Epoque>", "");
                    epo = epo.replaceFirst("</Epoque>", "");
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return epo ;
    }

    @Override
    public void save(String e, FileWriter f) {
        try {
            f.append("<Epoque>");
            f.append(e);
            f.append("</Epoque>") ;
            f.append('\n');
        } catch (IOException ex) {
            Logger.getLogger(EpoqueXMLDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
