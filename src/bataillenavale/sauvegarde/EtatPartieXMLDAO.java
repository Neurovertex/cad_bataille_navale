/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale.sauvegarde;

import bataillenavale.game.EtatPartie;
import bataillenavale.game.EtatPartie.Etat;
import bataillenavale.game.OptionsJeu;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Marie
 */
public class EtatPartieXMLDAO implements EtatPartieDAO {
    
    private static EtatPartieXMLDAO instance  = null;

    private EtatPartieXMLDAO() { }

    public static EtatPartieXMLDAO getInstance() {
        if (instance == null) {
            instance = new EtatPartieXMLDAO();
        }
        return instance;
    }

    @Override
    public EtatPartie load() {
        BufferedReader br = null;
        String line ;
        EtatPartie ep = null;
        try {
            br = new BufferedReader(new FileReader("sauvegarde.xml"));
            String etat = "" ;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("<Etat>")) {
                    etat = line.replaceFirst("<Etat>", "");
                    etat = etat.replaceFirst("</Etat>", "");
                } 
            }
            OptionsJeu oj = AbstractDAOFactory.getAbstractFactory(1).getOptionsJeuDAO().load();
            ep = new EtatPartie(oj);
            ep.setEtat(Etat.valueOf(etat)) ;
            ep.setGrilleIA(AbstractDAOFactory.getAbstractFactory(1).getGrilleDAO().load(false));
            ep.setGrilleJoueur(AbstractDAOFactory.getAbstractFactory(1).getGrilleDAO().load(true));
        } catch (FileNotFoundException e) {
            
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return ep ;
    }

    @Override
    public void save(EtatPartie ep) {
        try {
            FileWriter writer = new FileWriter("sauvegarde.xml");
            writer.append("<EtatPartie>") ;
            writer.append('\n');
            writer.append("<Etat>");
            writer.append(ep.getEtat().name());
            writer.append("</Etat>");
            writer.append('\n');
            //AbstractDAOFactory.getAbstractFactory(1).getEpoqueDAO().save(ep.getEpoque(), writer) ;
            //AbstractDAOFactory.getAbstractFactory(1).getStrategieIADAO().save(ep.getIa(), writer);
            AbstractDAOFactory.getAbstractFactory(1).getOptionsJeuDAO().save(ep.getOptions(), writer);
            AbstractDAOFactory.getAbstractFactory(1).getGrilleDAO().save(ep.getGrilleIA(), writer, false);
            AbstractDAOFactory.getAbstractFactory(1).getGrilleDAO().save(ep.getGrilleJoueur(), writer, true);
            writer.append("</EtatPartie>");
            writer.append('\n');
            writer.flush();
            writer.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    
}
