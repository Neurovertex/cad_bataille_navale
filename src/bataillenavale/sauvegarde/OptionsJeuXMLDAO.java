/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale.sauvegarde;

import bataillenavale.game.Epoque;
import bataillenavale.game.OptionsJeu;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Marie
 */
public class OptionsJeuXMLDAO implements OptionsJeuDAO {
    
    private static OptionsJeuXMLDAO instance  = null;

    private OptionsJeuXMLDAO() { }

    public static OptionsJeuXMLDAO getInstance() {
        if (instance == null) {
            instance = new OptionsJeuXMLDAO();
        }
        return instance;
    }

    @Override
    public OptionsJeu load() {
        BufferedReader br = null;
        String line ;
        OptionsJeu oj = null ;
        try {
            br = new BufferedReader(new FileReader("sauvegarde.xml"));
            String h = "" ;
            String l = "";
            String placement = "" ;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("<hauteur>")) {
                    h = line.replaceFirst("<hauteur>", "");
                    h = h.replaceFirst("</hauteur>", "");
                }
                if (line.startsWith("<largeur>")) {
                    l = line.replaceFirst("<largeur>", "");
                    l = l.replaceFirst("</largeur>", "");
                }
                if (line.startsWith("<placement>")) {
                    placement = line.replaceFirst("<placement>", "");
                    placement = placement.replaceFirst("</placement>", "");
                }
            }
            String ia = AbstractDAOFactory.getAbstractFactory(1).getStrategieIADAO().load();
            String epo = AbstractDAOFactory.getAbstractFactory(1).getEpoqueDAO().load();
            oj = new OptionsJeu(ia, Integer.parseInt(l), Integer.parseInt(h), epo, placement);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return oj ;
    }

    @Override
    public void save(OptionsJeu oj, FileWriter f) {
        try {
            f.append("<OptionsJeu>");
            f.append('\n');
            f.append("<hauteur>");
            f.append(oj.getHauteurGrille()+"");
            f.append("</hauteur>");
            f.append('\n');
            f.append("<largeur>");
            f.append(oj.getLargeurGrille()+"");
            f.append("</largeur>");
            f.append('\n');
            f.append("<placement>");
            f.append(oj.getPlacement());
            f.append("</placement>");
            f.append('\n');
            AbstractDAOFactory.getAbstractFactory(1).getEpoqueDAO().save(oj.getEpoque(), f);
            AbstractDAOFactory.getAbstractFactory(1).getStrategieIADAO().save(oj.getStrategieIA(), f);
            f.append("</OptionsJeu>");
            f.append('\n');
        } catch (IOException ex) {
            Logger.getLogger(OptionsJeuXMLDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
