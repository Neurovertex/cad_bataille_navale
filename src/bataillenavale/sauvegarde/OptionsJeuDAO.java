/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale.sauvegarde;

import bataillenavale.game.OptionsJeu;
import java.io.FileWriter;

/**
 *
 * @author Marie
 */
public interface OptionsJeuDAO {
    
    public OptionsJeu load();
    public void save(OptionsJeu oj, FileWriter f);
    
}
