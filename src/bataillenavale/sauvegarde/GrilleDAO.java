/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale.sauvegarde;

import bataillenavale.game.Grille;
import java.io.FileWriter;

/**
 *
 * @author Marie
 */
public interface GrilleDAO {
    
    public Grille load(boolean joueur) ;
    public void save(Grille g, FileWriter f, boolean joueur);
    
}
