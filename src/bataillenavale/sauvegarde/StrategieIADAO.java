/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale.sauvegarde;

import java.io.FileWriter;

/**
 *
 * @author Marie
 */
public interface StrategieIADAO {
    
    public String load();
    public void save(String s, FileWriter f);
    
}
