/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale.sauvegarde;

import bataillenavale.game.Coordonnees;
import bataillenavale.game.Grille;
import bataillenavale.game.Navire;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Marie
 */
public class GrilleNavireXMLDAO implements GrilleNavireDAO {
    
    private static GrilleNavireXMLDAO instance  = null;

    private GrilleNavireXMLDAO() { }

    public static GrilleNavireXMLDAO getInstance() {
        if (instance == null) {
            instance = new GrilleNavireXMLDAO();
        }
        return instance;
    }

    @Override
    public List<Grille.GrilleNavire> load(boolean joueur) {
        BufferedReader br = null;
        String line ;
        List<Grille.GrilleNavire> lgn = new ArrayList<>();
        try {
            br = new BufferedReader(new FileReader("sauvegarde.xml"));
            Grille.GrilleNavire gn = null ;
            boolean g = false ;
            Navire nav = null ;
            boolean p = false ;
            String x = "";
            String y = "";
            String rotation = "";
            Coordonnees c = null ;
            String tmp ;
            if (joueur) {
                tmp = "j";
            } else {
                tmp = "ia";
            }
            boolean b = false ;
            while ((line = br.readLine()) != null) {
                if (line.equals("<Grille"+tmp+">")) {
                    b = true ;
                }
                if (line.equals("</Grille"+tmp+">")) {
                    b = false ;
                }
                if (b) {
                    if (line.equals("<GrilleNavire>")) {
                        g = true;
                    }
                    if (line.equals("</GrilleNavire>")) {
                        g = false;
                        Grille gi = new Grille();
                        gn = gi.new GrilleNavire(nav, c);
                        lgn.add(gn);
                        nav = null;
                        c = null;
                        gn = null;
                    }
                    if (g) {
                        if (line.startsWith("<Navire>")) {
                            String n = line.replaceFirst("<Navire>", "");
                            n = n.replaceFirst("</Navire>", "");
                            nav = Navire.valueOf(n);
                        }
                        if (line.equals("<position>")) {
                            p = true;
                        }
                        if (line.equals("</position>")) {
                            p = false;
                            c = new Coordonnees(Integer.parseInt(x), Integer.parseInt(y), Integer.parseInt(rotation));
                            x = "";
                            y = "";
                            rotation = "";
                        }
                        if (p) {
                            if (line.startsWith("<x>")) {
                                x = line.replaceFirst("<x>", "");
                                x = x.replaceFirst("</x>", "");
                            }
                            if (line.startsWith("<y>")) {
                                y = line.replaceFirst("<y>", "");
                                y = y.replaceFirst("</y>", "");
                            }
                            if (line.startsWith("<rotation>")) {
                                rotation = line.replaceFirst("<rotation>", "");
                                rotation = rotation.replaceFirst("</rotation>", "");
                            }
                        }
                    }
                }
            }
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return lgn ;
    }

    @Override
    public void save(Grille.GrilleNavire gn, FileWriter f) {
        try {
            f.append("<GrilleNavire>");
            f.append('\n');
            f.append("<position>");
            f.append('\n');
            f.append("<x>");
            f.append(gn.getPosition().x+"");
            f.append("</x>");
            f.append('\n');
            f.append("<y>");
            f.append(gn.getPosition().y+"");
            f.append("</y>");
            f.append('\n');
            f.append("<rotation>");
            f.append(gn.getPosition().rotation+"");
            f.append("</rotation>");
            f.append('\n');
            f.append("</position>");
            f.append('\n');
            f.append("<Navire>");
            f.append(gn.getNavire().name());
            f.append("</Navire>");
            f.append('\n');
            f.append("</GrilleNavire>");
            f.append('\n');
        } catch (IOException ex) {
            Logger.getLogger(GrilleNavireXMLDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
