/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale.sauvegarde;

/**
 *
 * @author Marie
 */
public class ConcreteXMLFactory extends AbstractDAOFactory {

    @Override
    public EtatPartieDAO getEtatPartieDAO() {
        return EtatPartieXMLDAO.getInstance() ;
    }

    @Override
    public EpoqueDAO getEpoqueDAO() {
        return EpoqueXMLDAO.getInstance() ;
    }

    @Override
    public GrilleDAO getGrilleDAO() {
        return GrilleXMLDAO.getInstance();
    }

    @Override
    public OptionsJeuDAO getOptionsJeuDAO() {
        return OptionsJeuXMLDAO.getInstance();
    }

    @Override
    public StrategieIADAO getStrategieIADAO() {
        return StrategieIAXMLDAO.getInstance();
    }

    @Override
    public GrilleNavireDAO getGrilleNavireDAO() {
        return GrilleNavireXMLDAO.getInstance();
    }
    
}
