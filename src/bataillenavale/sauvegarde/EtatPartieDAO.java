/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale.sauvegarde;

import bataillenavale.game.EtatPartie;

/**
 *
 * @author Marie
 */
public interface EtatPartieDAO {
    
    public EtatPartie load() ;
    public void save(EtatPartie ep) ;
    
}
