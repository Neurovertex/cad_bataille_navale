/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale.sauvegarde;

import bataillenavale.game.StrategieIA;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Marie
 */
public class StrategieIAXMLDAO implements StrategieIADAO {
    
    private static StrategieIAXMLDAO instance  = null;

    private StrategieIAXMLDAO() { }

    public static StrategieIAXMLDAO getInstance() {
        if (instance == null) {
            instance = new StrategieIAXMLDAO();
        }
        return instance;
    }

    @Override
    public String load() {
        BufferedReader br = null;
        String line ;
        String ia = "";
        try {
            br = new BufferedReader(new FileReader("sauvegarde.xml"));
            while ((line = br.readLine()) != null) {
                if (line.startsWith("<StrategieIA>")) {
                    ia = line.replaceFirst("<StrategieIA>", "");
                    ia = ia.replaceFirst("</StrategieIA>", "");
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return ia ;
    }

    @Override
    public void save(String s, FileWriter f) {
        try {
            f.append("<StrategieIA>");
            f.append(s);
            f.append("</StrategieIA>");
            f.append('\n');
        } catch (IOException ex) {
            Logger.getLogger(StrategieIAXMLDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
