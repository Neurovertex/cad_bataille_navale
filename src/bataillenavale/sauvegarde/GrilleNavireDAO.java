/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale.sauvegarde;

import bataillenavale.game.Grille.GrilleNavire;
import java.io.FileWriter;
import java.util.List;

/**
 *
 * @author Marie
 */
public interface GrilleNavireDAO {
    
    public List<GrilleNavire> load(boolean joueur);
    public void save(GrilleNavire gn, FileWriter f);
    
}
