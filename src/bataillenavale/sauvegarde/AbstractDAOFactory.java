/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale.sauvegarde;

/**
 *
 * @author Marie
 */
public abstract class AbstractDAOFactory {
    
    public abstract EtatPartieDAO getEtatPartieDAO() ;
    public abstract EpoqueDAO getEpoqueDAO();
    public abstract GrilleDAO getGrilleDAO();
    public abstract OptionsJeuDAO getOptionsJeuDAO();
    public abstract StrategieIADAO getStrategieIADAO();
    public abstract GrilleNavireDAO getGrilleNavireDAO();
    
    public static AbstractDAOFactory getAbstractFactory(int sgbd) {
        switch (sgbd) {
            case 1 :
                return new ConcreteXMLFactory();
        }
        return null ;
    }
    
}
