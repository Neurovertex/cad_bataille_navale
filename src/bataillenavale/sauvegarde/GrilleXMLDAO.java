/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale.sauvegarde;

import bataillenavale.game.Epoque;
import bataillenavale.game.Grille;
import bataillenavale.game.Grille.GrilleNavire;
import bataillenavale.game.OptionsJeu;
import bataillenavale.game.epoque.EpoqueFactory;

import java.io.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Marie
 */
public class GrilleXMLDAO implements GrilleDAO {

	private static GrilleXMLDAO instance = null;

	private GrilleXMLDAO() {
	}

	public static GrilleXMLDAO getInstance() {
		if (instance == null) {
			instance = new GrilleXMLDAO();
		}
		return instance;
	}

	@Override
	public Grille load(boolean joueur) {
		BufferedReader br = null;
		String line;
		OptionsJeu oj = AbstractDAOFactory.getAbstractFactory(1).getOptionsJeuDAO().load();
		Epoque epo = EpoqueFactory.getEpoque(oj.getEpoque());
		Grille g = new Grille(oj.getLargeurGrille(), oj.getHauteurGrille(), epo);
		String tmp;
		if (joueur) {
			tmp = "j";
		} else {
			tmp = "ia";
		}
		boolean tirs[][] = new boolean[g.getWidth()][g.getHeight()];
		try {
			br = new BufferedReader(new FileReader("sauvegarde.xml"));
			boolean grille = false;
			boolean t = false;
			int i = 0;
			while ((line = br.readLine()) != null) {
				if (line.equals("<Grille" + tmp + ">")) {
					grille = true;
				}
				if (line.equals("</Grille" + tmp + ">")) {
					grille = false;
				}
				if (grille) {
					if (line.equals("<grilletirs>")) {
						t = true;
					}
					if (line.equals("</grilletirs>")) {
						t = false;
					}
					if (t && !line.equals("<grilletirs>")) {
						String[] ti = line.split(" ");
						for (int k = 0; k < g.getHeight(); k++) {
							if (ti[k].equals("1")) {
								tirs[i][k] = true;
							} else {
								tirs[i][k] = false;
							}
						}
						i++;
					}
				}
			}
			List<GrilleNavire> nav = AbstractDAOFactory.getAbstractFactory(1).getGrilleNavireDAO().load(joueur);
			g.setNavires(nav);
			g.setGrilleTirs(tirs);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return g;
	}

	@Override
	public void save(Grille g, FileWriter f, boolean joueur) {
		try {
			String tmp;
			if (joueur) {
				tmp = "j";
			} else {
				tmp = "ia";
			}
			f.append("<Grille" + tmp + ">");
			f.append('\n');
			f.append("<grilletirs>");
			f.append('\n');
			boolean[][] tirs = g.getGrilleTirs();
			for (int i = 0; i < g.getWidth(); i++) {
				for (int j = 0; j < g.getHeight(); j++) {
					if (tirs[i][j]) {
						f.append("1 ");
					} else {
						f.append("0 ");
					}
				}
				f.append('\n');
			}
			f.append("</grilletirs>");
			f.append('\n');
			List<GrilleNavire> l = g.getNavires();
			for (GrilleNavire gn : l) {
				AbstractDAOFactory.getAbstractFactory(1).getGrilleNavireDAO().save(gn, f);
			}
			f.append("</Grille" + tmp + ">");
			f.append('\n');
		} catch (IOException ex) {
			Logger.getLogger(GrilleXMLDAO.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

}
