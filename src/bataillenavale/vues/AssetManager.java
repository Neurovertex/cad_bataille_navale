package bataillenavale.vues;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Jeremie
 *         Date: 16/04/14, 20:25
 */
public class AssetManager {
	private static final AssetManager instance = new AssetManager();

	public static AssetManager getInstance() {
		return instance;
	}

	private static final String ASSETS_DIR = "assets";
	private final Map<String, BufferedImage> assets = new HashMap<>();

	private AssetManager() {

	}

	public void init() throws IOException {
		if (assets.size() > 0)
			return;
		File dir = new File(ASSETS_DIR);
		if (!dir.exists() || !dir.isDirectory() || !dir.canRead())
			throw new FileNotFoundException("Asset folder doesn't exist or can't be read.");

		File[] files = dir.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".png");
			}
		});

		for (File file : files) {
			String name = file.getName();
			name = name.substring(0, name.length() - 4);
			System.out.println("Loading " + name + " (" + file + ", " + file.length() / 1000 + "kB)");
			BufferedImage img = ImageIO.read(file);
			if (img != null)
				assets.put(name, img);
		}
		System.out.println("Successfully loaded " + assets.size() + " assets");
	}

	public Set<String> getAssets() {
		return Collections.unmodifiableSet(assets.keySet());
	}

	public BufferedImage getAsset(String name) {
		return assets.get(name);
	}

	public void putAsset(String name, BufferedImage image) {
		assets.put(name, image);
	}

	public boolean hasAsset(String name) {
		return assets.containsKey(name);
	}
}
