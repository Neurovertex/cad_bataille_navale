package bataillenavale.vues;

import bataillenavale.game.Coordonnees;
import bataillenavale.game.EtatPartie;
import bataillenavale.game.Grille;
import bataillenavale.game.Navire;
import bataillenavale.sauvegarde.AbstractDAOFactory;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.RGBImageFilter;
import java.util.*;
import java.util.List;

/**
 * @author Neurovertex
 *         Date: 14/04/14, 19:52
 */
public class VueJeu extends JPanel implements Observer {
	private EtatPartie etat;
	private Coordonnees current;
	private boolean grilleIA;
	private int rotation = 0;
	private List<Navire> toPlace = new ArrayList<>();

	private JLabel naviresRestantIA, naviresRestantJoueur, status;
	private final JButton options, quit, sauvegarde;

	private static final Map<EtatPartie.Etat, String> etatMessages = new HashMap<>();

	static {
		etatMessages.put(EtatPartie.Etat.PLACEMENT, "Clic gauche pour placer un navire, clic droit pour tourner");
		etatMessages.put(EtatPartie.Etat.TOUR_JOUEUR, "Clique gauche pour tirer sur une case adverse");
		etatMessages.put(EtatPartie.Etat.MATCH_NUL, "Match nul");
		etatMessages.put(EtatPartie.Etat.VICTOIRE_IA, "L'IA a gagné");
		etatMessages.put(EtatPartie.Etat.VICTOIRE_JOUEUR, "Vous avez gagné!");
	}

	public VueJeu(final FenetrePrincipale fenetre, EtatPartie etat1) {
		super();
		this.etat = etat1;
		setBorder(new EmptyBorder(5, 5, 5, 5));
		etat.addObserver(this);
		etat.getGrilleJoueur().addObserver(this);
		etat.getGrilleIA().addObserver(this);

		for (Navire nav : Navire.values())
			for (int i = 0; i < nav.getNombreMax(); i++) {
				toPlace.add(nav);
			}

		for (Grille.GrilleNavire nav : etat.getGrilleJoueur().getNavires())
			toPlace.remove(nav.getNavire());

		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = constraints.gridy = 0;
		constraints.gridwidth = constraints.gridheight = 1;
		constraints.weightx = constraints.weighty = 1;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.insets = new Insets(5, 5, 5, 5);


		add(new JLabel("Joueur", SwingConstants.CENTER), constraints);
		constraints.gridy = 1;
		add(naviresRestantJoueur = new JLabel("", SwingConstants.CENTER), constraints);

		constraints.gridx = 1;
		constraints.gridy = 0;
		add(new JLabel("Navires restants", SwingConstants.CENTER), constraints);

		constraints.gridy = 0;
		constraints.gridx = 2;
		add(new JLabel("IA", SwingConstants.CENTER), constraints);
		constraints.gridy = 1;
		add(naviresRestantIA = new JLabel("", SwingConstants.CENTER), constraints);


		constraints.anchor = GridBagConstraints.CENTER;
		constraints.gridy++;
		constraints.gridx = 0;
		constraints.gridwidth = 3;
		constraints.weightx = 3;
		constraints.fill = GridBagConstraints.NONE;
		VueGrille grid = new VueGrille();
		constraints.insets = new Insets((grid.MAX_HEIGHT - grid.gridHeight) / 2,
				(grid.MAX_WIDTH - grid.gridWidth) / 2,
				(grid.MAX_HEIGHT - grid.gridHeight) / 2,
				(grid.MAX_WIDTH - grid.gridWidth) / 2);
		add(grid, constraints);

		ActionListener listener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == quit)
					fenetre.close();
				if (e.getSource() == sauvegarde)
					AbstractDAOFactory.getAbstractFactory(1).getEtatPartieDAO().save(etat);
				if (e.getSource() == options) {
					if (!etat.isFini())
						fenetre.gotoOptionsJeu();
					else
						fenetre.gotoNouvellePartie();
				}
			}
		};
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.gridwidth = 1;
		constraints.weightx = 1;
		options = new JButton("Options");
		constraints.gridy++;
		options.addActionListener(listener);
		add(options, constraints);
		sauvegarde = new JButton("Sauvegarder");
		constraints.gridx++;
		sauvegarde.addActionListener(listener);
		add(sauvegarde, constraints);
		quit = new JButton("Quitter");
		constraints.gridx++;
		quit.addActionListener(listener);
		add(quit, constraints);

		constraints.gridx = 0;
		constraints.gridy++;
		constraints.gridwidth = 3;
		constraints.weightx = 3;
		add(status = new JLabel("", SwingConstants.CENTER), constraints);


		updateVue();
	}

	@Override
	public void update(Observable o, Object arg) {
		if (o == etat.getGrilleJoueur() && arg instanceof Grille.GrilleNavire) {
			toPlace.remove(((Grille.GrilleNavire) arg).getNavire());
		}
		updateVue();
	}

	private void updateVue() {
		int cnt = 0;
		for (Grille.GrilleNavire nav : etat.getGrilleJoueur().getNavires())
			if (!nav.isCoule())
				cnt++;
		naviresRestantJoueur.setText(String.format("%d/%d", cnt, EtatPartie.TOTAL_NAVIRES_MAX));
		cnt = 0;
		for (Grille.GrilleNavire nav : etat.getGrilleIA().getNavires())
			if (!nav.isCoule())
				cnt++;
		naviresRestantIA.setText(String.format("%d/%d", cnt, EtatPartie.TOTAL_NAVIRES_MAX));

		status.setText(etatMessages.containsKey(etat.getEtat()) ? etatMessages.get(etat.getEtat()) : "");
		if (etat.isFini()) {
			options.setEnabled(false);
			if (etat.getEtat() == EtatPartie.Etat.VICTOIRE_JOUEUR) {
				status.setForeground(new Color(0, 128, 0));
			} else if (etat.getEtat() == EtatPartie.Etat.VICTOIRE_IA) {
				status.setForeground(new Color(128, 0, 0));
			} else {
				status.setBackground(new Color(64, 64, 64));
			}
		}
		repaint();
	}

	public EtatPartie getEtat() {
		return etat;
	}

	private class VueGrille extends JPanel {
		private final int MAX_WIDTH = 250, MAX_HEIGHT = 250;
		private final int gridWidth, gridHeight;
		private int scale;
		BufferedImage aim, hit, miss;
		private Map<Navire, BufferedImage> navireAssets = new HashMap<>();

		private VueGrille() {
			ControllerJeu controller = new ControllerJeu();
			addMouseListener(controller);
			addMouseMotionListener(controller);
			AssetManager assets = AssetManager.getInstance();
			scale = Math.min(MAX_WIDTH / etat.getOptions().getLargeurGrille(),
					MAX_HEIGHT / etat.getOptions().getHauteurGrille());
			gridWidth = etat.getOptions().getLargeurGrille() * scale;
			gridHeight = etat.getOptions().getHauteurGrille() * scale;
			setPreferredSize(new Dimension(gridWidth, gridHeight * 2));

			aim = resizeImage(assets.getAsset("aim"), scale, scale);
			hit = resizeImage(assets.getAsset("hit"), scale, scale);
			miss = resizeImage(assets.getAsset("miss"), scale, scale);

			for (Navire nav : Navire.values()) {
				navireAssets.put(nav, resizeImage(etat.getEpoque().getAppearance(nav), scale * nav.getTaille(), scale));
			}
		}

		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			BufferedImage img = new BufferedImage(gridWidth, gridHeight, BufferedImage.TYPE_INT_ARGB);
			paintGrid(img.createGraphics(), etat.getGrilleIA(), etat.getGrilleJoueur(), img.getWidth(), img.getHeight(), true);
			g.drawImage(img, 0, 0, null);
			paintGrid(img.createGraphics(), etat.getGrilleJoueur(), etat.getGrilleIA(), img.getWidth(), img.getHeight(), false);
			g.drawImage(img, 0, gridHeight, null);
		}

		protected void paintGrid(Graphics g, Grille grille, Grille portee, int width, int height, boolean ia) {
			super.paintComponent(g);
			g.setColor(ia ? new Color(0x80, 0x00, 0x00) : new Color(0x00, 0x00, 0x80));
			int w = grille.getWidth(), h = grille.getHeight();
			g.drawRect(0, 0, width - 1, height - 1);

			for (int i = 1; i < w; i++) // Grille
				g.drawRect(i * scale - 1, 0, 1, height - 1);

			for (int i = 1; i < h; i++)
				g.drawRect(0, i * scale - 1, width - 1, 1);

			g.setColor(new Color(0, 0, 0x80, 0x20));

			if (ia || etat.isFini()) // Portee
				for (int i = 0; i < w; i++)
					for (int j = 0; j < h; j++)
						if (portee.canTir(new Coordonnees(i, j)))
							g.fillRect(i * scale, j * scale, scale, scale);

			g.setColor(Color.BLACK);

			if (!ia || etat.isFini()) { // Navires
				for (Grille.GrilleNavire nav : grille.getNavires()) {
					Coordonnees start = nav.getPosition();
					int xfact = (int) Math.round(Math.cos(start.rotation * Math.PI / 2)), yfact = (int) Math.round(Math.sin(start.rotation * Math.PI / 2));
					Coordonnees end = new Coordonnees(start.x + (nav.getNavire().getTaille() - 1) * xfact, start.y + (nav.getNavire().getTaille() - 1) * yfact);
					BufferedImage asset = rotateAsset(navireAssets.get(nav.getNavire()), start.rotation);
					if (asset != null) {
						if (nav.isCoule())
							asset = makeTransparent(asset, 0x80);
						g.drawImage(asset, Math.min(start.x, end.x) * scale, Math.min(start.y, end.y) * scale, null);
					} else {
						int rad = Math.min(scale, scale) / 2;
						g.fillRect(Math.min(start.x, end.x) * scale + scale / 2 - Math.abs(yfact * rad / 2),
								Math.min(start.y, end.y) * scale + scale / 2 - Math.abs(xfact * rad / 2),
								Math.abs(end.x - start.x) * scale + Math.abs(yfact * rad),
								Math.abs(end.y - start.y) * scale + Math.abs(xfact * rad));
						g.fillOval(start.x * scale + scale / 2 - rad / 2, start.y * scale + scale / 2 - rad / 2, rad, rad);
						g.fillOval(end.x * scale + scale / 2 - rad / 2, end.y * scale + scale / 2 - rad / 2, rad, rad);
					}
				}
				if (etat.getEtat() == EtatPartie.Etat.PLACEMENT && current != null && !toPlace.isEmpty() && !grilleIA) { // Navire "fantome" pendant le placement
					boolean canAdd = grille.canAdd(toPlace.get(0), current);
					g.setColor(canAdd ? new Color(0x80, 0x80, 0x80, 0x80) : new Color(0xFF, 0, 0, 0x80));
					Coordonnees start = current;
					int xfact = (int) Math.round(Math.cos(start.rotation * Math.PI / 2)), yfact = (int) Math.round(Math.sin(start.rotation * Math.PI / 2));
					Coordonnees end = new Coordonnees(start.x + (toPlace.get(0).getTaille() - 1) * xfact, start.y + (toPlace.get(0).getTaille() - 1) * yfact);
					BufferedImage asset = rotateAsset(navireAssets.get(toPlace.get(0)), start.rotation);
					if (asset != null) {
						if (!canAdd)
							asset = makeTransparent(asset, 0x80);
						g.drawImage(asset, Math.min(start.x, end.x) * scale, Math.min(start.y, end.y) * scale, null);
					} else {
						int rad = Math.min(scale, scale) / 2;
						g.fillRect(Math.min(start.x, end.x) * scale + scale / 2 - Math.abs(yfact * rad / 2),
								Math.min(start.y, end.y) * scale + scale / 2 - Math.abs(xfact * rad / 2),
								Math.abs(end.x - start.x) * scale + Math.abs(yfact * rad),
								Math.abs(end.y - start.y) * scale + Math.abs(xfact * rad));
						g.fillOval(start.x * scale + scale / 2 - rad / 2, start.y * scale + scale / 2 - rad / 2, rad, rad);
						g.fillOval(end.x * scale + scale / 2 - rad / 2, end.y * scale + scale / 2 - rad / 2, rad, rad);
					}
				}
			}

			for (int i = 0; i < w; i++) // Touches et manques
				for (int j = 0; j < h; j++)
					if (grille.getTir(new Coordonnees(i, j)))
						g.drawImage(grille.getNavire(new Coordonnees(i, j)) != null ? hit : miss, i * scale, j * scale, null);

			// Viseur
			if (ia && current != null && grilleIA && etat.getEtat() == EtatPartie.Etat.TOUR_JOUEUR)
				if (portee.canTir(current) && !grille.getTir(current)) {
					g.setColor(Color.RED);
					g.drawImage(aim, current.x * scale, current.y * scale, null);
				}
		}

		private BufferedImage resizeImage(BufferedImage src, int width, int height) {
			if (src == null)
				return null;
			int w = src.getWidth(), h = src.getHeight();
			if (w != width || h != height) {
				BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
				AffineTransform transform = AffineTransform.getScaleInstance(width / (double) w, height / (double) h);
				img.createGraphics().drawRenderedImage(src, transform);
				return img;
			} else
				return src;
		}

		private BufferedImage rotateAsset(BufferedImage src, int rotation) {
			if (src == null)
				return null;
			BufferedImage dst = (rotation % 2 == 0) ? new BufferedImage(src.getWidth(), src.getHeight(), BufferedImage.TYPE_INT_ARGB) : new BufferedImage(src.getHeight(), src.getWidth(), BufferedImage.TYPE_INT_ARGB);
			AffineTransform transform = AffineTransform.getTranslateInstance(dst.getWidth() / 2, dst.getHeight() / 2);

			if (rotation % 2 == 1) {
				transform.concatenate(AffineTransform.getQuadrantRotateInstance(rotation));
			}
			if (rotation > 1) {
				transform.concatenate(AffineTransform.getScaleInstance(-1, 1));
			}

			transform.concatenate(AffineTransform.getTranslateInstance(-src.getWidth() / 2, -src.getHeight() / 2));

			dst.createGraphics().drawRenderedImage(src, transform);
			return dst;
		}

		private BufferedImage makeTransparent(BufferedImage src, final int transparency) {
			RGBImageFilter filter = new RGBImageFilter() {
				@Override
				public int filterRGB(int x, int y, int rgb) {
					return rgb & (0x00FFFFFF | ((transparency & 0xFF) << 24));
				}
			};
			FilteredImageSource source = new FilteredImageSource(src.getSource(), filter);
			Image filteredImage = Toolkit.getDefaultToolkit().createImage(source);
			BufferedImage img = new BufferedImage(src.getWidth(), src.getHeight(), src.getType());
			img.createGraphics().drawImage(filteredImage, 0, 0, null);
			return img;
		}

		private class ControllerJeu implements MouseListener, MouseMotionListener {

			@Override
			public void mouseClicked(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON3)
					rotation = (rotation + 1) % 4;
				mouseMoved(e); // S'assurer que current est a jour
				if (e.getButton() == MouseEvent.BUTTON1 && current != null) {
					if (etat.getEtat() == EtatPartie.Etat.TOUR_JOUEUR && grilleIA && etat.getGrilleJoueur().canTir(current) && !etat.getGrilleIA().getTir(current)) {
						etat.jouer(current);
					} else if (etat.getEtat() == EtatPartie.Etat.PLACEMENT && !grilleIA && !toPlace.isEmpty()) {
						etat.placer(current, toPlace.get(0));
					}
				}
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
				current = null;
			}

			@Override
			public void mouseDragged(MouseEvent e) {
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				int x = e.getX(), y = e.getY();
				int w = etat.getGrilleJoueur().getWidth(), h = etat.getGrilleJoueur().getHeight(), xscale = gridWidth / w, yscale = gridHeight / h;
				Coordonnees coord;
				if (x < gridWidth && y < gridHeight) {
					grilleIA = true;
					coord = new Coordonnees(x / xscale, y / yscale, rotation);
				} else if (x < gridWidth && y < 2 * gridHeight) {
					grilleIA = false;
					coord = new Coordonnees(x / xscale, (y - gridHeight) / yscale, rotation);
				} else {
					coord = null;
				}
				if (coord == null && current != null || coord != null && etat.getGrilleJoueur().coordonneesValides(coord) && !coord.equals(current)) {
					repaint();
					current = coord;
				}
			}
		}

	}
}
