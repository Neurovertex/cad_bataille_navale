/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale.vues;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Marie
 */
public class MenuPrincipal extends JPanel {
	private FenetrePrincipale fenetre;

	public MenuPrincipal(FenetrePrincipale fenetre) {
		this.fenetre = fenetre;
		build();
	}

	private void build() {
		setLayout(new GridBagLayout());
		setBorder(new EmptyBorder(10, 10, 10, 10));
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = constraints.gridy = 0;
		constraints.insets = new Insets(25, 25, 25, 25);
		constraints.fill = GridBagConstraints.BOTH;

		JLabel jl1 = new JLabel("Menu principal");
		jl1.setHorizontalAlignment(JLabel.CENTER);
		add(jl1, constraints);

		JLabel jl2 = new JLabel("Bataille navale");
		jl2.setHorizontalAlignment(JLabel.CENTER);
		constraints.gridy++;
		add(jl2, constraints);

		JButton nvo = new JButton("Nouvelle partie");
		nvo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				fenetre.gotoNouvellePartie();
			}
		});
		constraints.gridy++;
		add(nvo, constraints);

		JButton charger = new JButton("Charger une partie");
		charger.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
                            fenetre.gotoChargerPartie();
                        }
		});
		constraints.gridy++;
		add(charger, constraints);

		JButton quitter = new JButton("Quitter");
		quitter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fenetre.close();
			}
		});
		constraints.gridy++;
		add(quitter, constraints);
	}

}
