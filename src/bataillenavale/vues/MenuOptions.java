/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale.vues;

import bataillenavale.game.EtatPartie;
import bataillenavale.game.OptionsJeu;
import bataillenavale.game.epoque.EpoqueFactory;
import bataillenavale.game.ia.StrategieIAFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.prefs.BackingStoreException;

/**
 * @author Marie
 */
public class MenuOptions extends JPanel {
	private FenetrePrincipale fenetre;
	private EtatPartie etat;

	private JRadioButton randomPlacement, manualPlacement;
	private JSpinner gridWidth, gridHeight;
	private JButton confirm;
	private JComboBox<String> epoque, strategie;

	public MenuOptions(FenetrePrincipale fenetre) {
		this.fenetre = fenetre;
		build();
		updateOptions(new OptionsJeu());
	}

	public MenuOptions(FenetrePrincipale fenetre, EtatPartie etat) {
		this.fenetre = fenetre;
		this.etat = etat;
		build();
		updateOptions(etat.getOptions());
	}

	private void build() {
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridwidth = 3;
		constraints.weightx = constraints.weighty = 1;
		constraints.gridx = 0;
		constraints.fill = GridBagConstraints.NONE;
		constraints.insets = new Insets(10, 10, 10, 10);
		constraints.anchor = GridBagConstraints.CENTER;
		JLabel jl1 = new JLabel("Nouvelle partie", SwingConstants.CENTER);
		constraints.gridy = 0;
		add(jl1, constraints);
		JLabel jl2 = new JLabel("Stratégie de l'ordinateur :", SwingConstants.CENTER);
		constraints.gridy++;
		add(jl2, constraints);

		boolean protect = (etat == null); // Options non editables si le jeu est en cours


		// Taille de la grille
		JLabel jl3 = new JLabel("Taille de la grille :", SwingConstants.CENTER);
		constraints.gridy++;
		add(jl3, constraints);
		gridWidth = new JSpinner(new SpinnerNumberModel(10, 6, 20, 1));
		gridWidth.setEnabled(protect);
		constraints.gridwidth = 1;
		constraints.anchor = GridBagConstraints.EAST;
		constraints.gridy++;
		constraints.gridx = 0;
		add(gridWidth, constraints);
		JLabel x = new JLabel("X", SwingConstants.CENTER);
		constraints.gridx = 1;
		constraints.weightx = 0;
		constraints.anchor = GridBagConstraints.CENTER;
		add(x, constraints);
		gridHeight = new JSpinner(new SpinnerNumberModel(10, 6, 20, 1));
		gridHeight.setEnabled(protect);
		constraints.gridx = 2;
		constraints.weightx = 1;
		constraints.anchor = GridBagConstraints.WEST;
		add(gridHeight, constraints);

		// Strategie IA
		strategie = new JComboBox<>();
		for (String str : StrategieIAFactory.getStrategies())
			strategie.addItem(StrategieIAFactory.getStrategie(str).getNom());
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.gridwidth = 3;
		constraints.gridx = 0;
		constraints.gridy++;
		add(strategie, constraints);

		// Epoque
		JLabel jl4 = new JLabel("Epoque :", SwingConstants.CENTER);
		constraints.gridy++;
		add(jl4, constraints);
		epoque = new JComboBox<>();
		for (String str : EpoqueFactory.getEpoques())
			epoque.addItem(EpoqueFactory.getEpoque(str).getNom());
		epoque.setEnabled(protect);
		constraints.gridy++;
		add(epoque, constraints);

		// Placement
		JLabel jl5 = new JLabel("Placement des navires", SwingConstants.CENTER);
		constraints.gridy++;
		add(jl5, constraints);
		ButtonGroup place = new ButtonGroup();
		manualPlacement = new JRadioButton("Manuel");
		manualPlacement.setEnabled(protect);
		place.add(manualPlacement);
		constraints.gridy++;
		add(manualPlacement, constraints);
		randomPlacement = new JRadioButton("Aléatoire");
		randomPlacement.setEnabled(protect);
		place.add(randomPlacement);
		constraints.gridy++;
		add(randomPlacement, constraints);
		confirm = new JButton("Jouer");
		confirm.addActionListener(new MenuOptionsController());
		constraints.gridy++;
		add(confirm, constraints);
	}

	public void updateOptions(OptionsJeu options) {

		if (options.getPlacement().equalsIgnoreCase("random"))
			randomPlacement.setSelected(true);
		else
			manualPlacement.setSelected(true);

		epoque.setSelectedItem(EpoqueFactory.getEpoque(options.getEpoque()).getNom());
		strategie.setSelectedItem(StrategieIAFactory.getStrategie(options.getStrategieIA()).getNom());

		gridWidth.setValue(options.getLargeurGrille());
		gridHeight.setValue(options.getHauteurGrille());
	}

	private class MenuOptionsController implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == confirm) {
				String placement, strat, epoqueName;
				Integer width, height;
				placement = (randomPlacement.isSelected()) ? "random" : "manual";

				strat = (String) strategie.getSelectedItem();
				epoqueName = (String) epoque.getSelectedItem();

				width = (Integer) gridWidth.getValue();
				height = (Integer) gridHeight.getValue();

				for (String str : EpoqueFactory.getEpoques())
					if (epoqueName.equals(EpoqueFactory.getEpoque(str).getNom()))
						epoqueName = str;

				for (String str : StrategieIAFactory.getStrategies())
					if (strat.equals(StrategieIAFactory.getStrategie(str).getNom()))
						strat = str;

				OptionsJeu options = new OptionsJeu(strat, width, height, epoqueName, placement);
				try {
					options.saveOptions();
				} catch (BackingStoreException e1) {
					System.err.println();
				}
				if (etat != null) {
					etat.setOptions(options);
				} else {
					fenetre.nouvellePartie(options);
				}
				fenetre.gotoVueJeu();
			}
		}
	}

}
