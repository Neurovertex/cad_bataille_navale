package bataillenavale.vues;

import bataillenavale.game.BoucleJeu;
import bataillenavale.game.EtatPartie;
import bataillenavale.game.OptionsJeu;
import bataillenavale.sauvegarde.AbstractDAOFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Jeremie
 *         Date: 16/04/14, 17:16
 */
public class FenetrePrincipale {
	private JFrame window;
	private MenuPrincipal menuPrincipal = new MenuPrincipal(this);
	private VueJeu vueJeu;

	public FenetrePrincipale() {
		window = new JFrame("Bataille Navale");
		window.setContentPane(menuPrincipal);
		window.setResizable(false);
		window.pack();
		window.setVisible(true);
		window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		window.setMinimumSize(new Dimension(300, 500));
		window.setIconImage(AssetManager.getInstance().getAsset("icon"));
		FenetrePrincipaleController controller = new FenetrePrincipaleController();

		JMenuBar menuBar = new JMenuBar();
		JMenu jeu = new JMenu("Jeu");
		jeu.add(new JMenuItem("Nouvelle partie")).addActionListener(controller);
		jeu.add(new JMenuItem("Charger Partie")).addActionListener(controller);
		jeu.add(new JMenuItem("Menu Principal")).addActionListener(controller);
		jeu.add(new JMenuItem("Quitter")).addActionListener(controller);

		menuBar.add(jeu);

		window.setJMenuBar(menuBar);
	}

	public void gotoMenuPrincipal() {
		window.setContentPane(menuPrincipal);
		window.pack();
	}

	public void gotoNouvellePartie() {
		window.setContentPane(new MenuOptions(this));
		window.pack();
	}

	public void gotoChargerPartie() {
		EtatPartie etat = AbstractDAOFactory.getAbstractFactory(1).getEtatPartieDAO().load();
                if (etat != null) {
                    if (vueJeu != null)
                            vueJeu.getEtat().stop();
                    vueJeu = new VueJeu(this, etat);
                    new BoucleJeu(etat).start();
                    gotoVueJeu();
                } else {
                    JOptionPane.showMessageDialog(window, "Impossible de charger une partie car aucune partie n'a été sauvegardée", "Chargement impossible", JOptionPane.ERROR_MESSAGE, null);
                }
	}

	public void nouvellePartie(OptionsJeu options) {
		if (vueJeu != null)
			vueJeu.getEtat().stop();
		vueJeu = new VueJeu(this, new EtatPartie(options));
		new BoucleJeu(vueJeu.getEtat()).start();
	}

	public void gotoOptionsJeu() {
		if (vueJeu == null)
			return;
		window.setContentPane(new MenuOptions(this, vueJeu.getEtat()));
		window.pack();
	}

	public void gotoVueJeu() {
		if (vueJeu == null)
			nouvellePartie(new OptionsJeu());
		window.setContentPane(vueJeu);
		window.pack();
	}

	public void close() {
		window.setVisible(false);
		System.exit(0);
	}

	private class FenetrePrincipaleController implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof JMenuItem) {
				JMenuItem item = (JMenuItem) e.getSource();
				switch (item.getText()) {
					case "Nouvelle partie":
						gotoNouvellePartie();
						break;
					case "Quitter":
						close();
						break;
					case "Charger Partie":
						gotoChargerPartie();
						break;
					case "Menu Principal":
						gotoMenuPrincipal();
						break;
					default:
						System.err.println("Recognized menu item : " + item.getText());
						break;
				}
			}
		}
	}
}
