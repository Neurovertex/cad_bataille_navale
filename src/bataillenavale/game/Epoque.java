package bataillenavale.game;

import java.awt.image.BufferedImage;

public interface Epoque {
	public abstract String getNom();

	public abstract String getNomNavire(Navire n);

	public abstract int getFirepower(Navire n);

	public abstract BufferedImage getAppearance(Navire n);
}
