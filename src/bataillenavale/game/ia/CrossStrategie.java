/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale.game.ia;

import bataillenavale.game.Coordonnees;
import bataillenavale.game.EtatPartie;
import bataillenavale.game.Grille;
import bataillenavale.game.StrategieIA;

/**
 * @author Marie
 */
public class CrossStrategie implements StrategieIA {
	public static void register() {
		StrategieIAFactory.register("cross", new CrossStrategie());
	}

	private CrossStrategie() {
	}

    @Override
    public Coordonnees jouer(EtatPartie etat) {
        Grille tirs = etat.getGrilleJoueur(), portee = etat.getGrilleIA();
        int w = tirs.getWidth(), h = tirs.getHeight();
        //on recherche si des navires sont deja touchés
        for (int i = 0 ; i < w ; i++) {
            for (int j = 0 ; j < h ; j++) {
                Coordonnees c = new Coordonnees(i, j) ;
                if (tirs.getNavire(c) != null && tirs.getTir(c)) {
                    c = new Coordonnees(i+1, j);
                    if (i+1 < w && portee.canTir(c) && !tirs.getTir(c)) {
                        return c;
                    }
                    c = new Coordonnees(i, j+1);
                    if (j+1 < h && portee.canTir(c) && !tirs.getTir(c)) {
                        return c;
                    }
                    c = new Coordonnees(i-1, j);
                    if (i-1 >= 0 && portee.canTir(c) && !tirs.getTir(c)) {
                        return c;
                    }
                    c = new Coordonnees(i, j-1);
                    if (j-1 >= 0 && portee.canTir(c) && !tirs.getTir(c)) {
                        return c;
                    }
                }
            }
        }
        //si aucun navire n'est touche, alors on parcourt la grille une case sur deux jusqu'a trouver une case ou on peut tirer
        for (int i = 0; i < w; i++) {
            for (int j = i % 2; j < h; j += 2) {
                Coordonnees coord = new Coordonnees(i, j);
                if (portee.canTir(coord) && !tirs.getTir(coord)) {
                    return coord;
                }
            }
        }
        //si on a tire sur toutes les cases une sur deux, on cherche une autre case ou tirer
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                Coordonnees coord = new Coordonnees(i, j);
                if (portee.canTir(coord) && !tirs.getTir(coord)) {
                    return coord;
                }
            }
        }
        //on ne peut plus tirer nul part
        return null;
    }

	@Override
	public String getNom() {
		return "En croix";
	}

}
