package bataillenavale.game.ia;

import bataillenavale.game.Coordonnees;
import bataillenavale.game.EtatPartie;
import bataillenavale.game.Grille;
import bataillenavale.game.StrategieIA;

/**
 * @author Jeremie
 *         Date: 06/04/14, 18:03
 */
public class RandomStrategie implements StrategieIA {
	public static void register() {
		StrategieIAFactory.register("random", new RandomStrategie());
	}

	private RandomStrategie() {
	}

	@Override
	public Coordonnees jouer(EtatPartie etat) {
		Grille tirs = etat.getGrilleJoueur(), portee = etat.getGrilleIA();
		int w = tirs.getWidth(), h = tirs.getHeight();
		boolean[][] grille = new boolean[w][h];
		int nb = 0;
		Coordonnees coord;

		for (int i = 0; i < w; i++)
			for (int j = 0; j < h; j++) {
				coord = new Coordonnees(i, j);
				if (grille[i][j] = portee.canTir(coord) && !tirs.getTir(coord))
					nb++;
			}
		if (nb == 0)
			return null; // Ne peut pas jouer
		int rnd = (int) (Math.random() * nb), i = 0;

		while (rnd > 0 || !grille[i%w][i/w]) {
			if (grille[i%w][i/w])
				rnd --;
			i++;
		}

		return new Coordonnees(i%w, i/w);
	}

	@Override
	public String getNom() {
		return "Aléatoire";
	}
}
