package bataillenavale.game.ia;

import bataillenavale.game.StrategieIA;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Neurovertex
 *         Date: 21/04/2014, 19:12
 */
public class StrategieIAFactory {

	private static final Map<String, StrategieIA> strategies = new HashMap<>();

	public static void register(String name, StrategieIA strategie) {
		strategies.put(name, strategie);
	}

	public static StrategieIA getStrategie(String name) {
		return strategies.get(name.toLowerCase());
	}

	public static Set<String> getStrategies() {
		return new HashSet<>(strategies.keySet());
	}

}
