/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillenavale.game.epoque;

import bataillenavale.game.Epoque;
import bataillenavale.game.Navire;
import bataillenavale.vues.AssetManager;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Marie
 */
public class Renaissance implements Epoque {
	public static void register() {
		EpoqueFactory.register("renaissance", new Renaissance());
	}

	private final Map<Navire, BufferedImage> assets = new HashMap<>();

	private Renaissance() {
		AssetManager manager = AssetManager.getInstance();
		try {
			manager.init();
		} catch (IOException e) {
			System.err.println("Can't load assets");
			e.printStackTrace();
			System.exit(0);
		}
		BufferedImage asset = manager.getAsset("renaissance");
		int step = 100, cnt = 0;
		for (Navire nav : Navire.values()) {
			BufferedImage img = asset.getSubimage(0, cnt * step, nav.getTaille() * step, step);
			assets.put(nav, img);
			cnt++;
		}
	}

    @Override
    public String getNom() {
        return "Renaissance" ;
    }

    @Override
    public String getNomNavire(Navire n) {
        switch (n) {
            case TINY:
                return "Cogue";
            case SMALL:
                return "Voilier";
            case MEDIUM:
                return "Caraque";
            case LARGE:
                return "Caravelle";
            case HUGE:
                return "Galion";
        }
        return "<Erreur>";
    }

    @Override
    public int getFirepower(Navire n) {
        switch (n) {
            case TINY:
                return 1;
            case SMALL:
                return 2;
            case MEDIUM:
                return 3;
            case LARGE:
                return 4;
            case HUGE:
                return 5;
        }
        return -1;
    }

	@Override
	public BufferedImage getAppearance(Navire n) {
		return assets.get(n);
	}

}
