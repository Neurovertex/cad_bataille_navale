package bataillenavale.game.epoque;

import bataillenavale.game.Epoque;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Neurovertex
 *         Date: 21/04/2014, 18:58
 */
public class EpoqueFactory {

	private static Map<String, Epoque> epoques = new HashMap<>();

	private EpoqueFactory() {
	}

	public static void register(String nom, Epoque epoque) {
		epoques.put(nom, epoque);
	}

	public static Epoque getEpoque(String nom) {
		return epoques.get(nom.toLowerCase());
	}

	public static Set<String> getEpoques() {
		return new HashSet<>(epoques.keySet()); // Copy for thread-safety
	}
}
