package bataillenavale.game.epoque;

import bataillenavale.game.Epoque;
import bataillenavale.game.Navire;
import bataillenavale.vues.AssetManager;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Jeremie
 *         Date: 06/04/14, 18:31
 */
public class EpoqueModerne implements Epoque {

	public static void register() {
		EpoqueFactory.register("moderne", new EpoqueModerne());
	}

	private final Map<Navire, BufferedImage> assets = new HashMap<>();

	private EpoqueModerne() {
		AssetManager manager = AssetManager.getInstance();
		try {
			manager.init();
		} catch (IOException e) {
			System.err.println("Can't load assets");
			e.printStackTrace();
			System.exit(0);
		}
		BufferedImage asset = manager.getAsset("moderne");
		int step = 100, cnt = 0;
		for (Navire nav : Navire.values()) {
			BufferedImage img = asset.getSubimage(0, cnt * step, nav.getTaille() * step, step);
			assets.put(nav, img);
			cnt++;
		}
	}

	@Override
	public String getNom() {
		return "Epoque Moderne";
	}

	@Override
	public String getNomNavire(Navire n) {
		switch (n) {
			case TINY:
				return "Patrouilleur";
			case SMALL:
				return "Destroyer";
			case MEDIUM:
				return "Sous-marin";
			case LARGE:
				return "Cuirassé";
			case HUGE:
				return "Porte-avion";
		}
		return "<Erreur>";
	}

	@Override
	public int getFirepower(Navire n) {
		switch (n) {
			case TINY:
				return 2;
			case SMALL:
				return 2;
			case MEDIUM:
				return 3;
			case LARGE:
				return 5;
			case HUGE:
				return 7;
		}
		return -1;
	}

	@Override
	public BufferedImage getAppearance(Navire n) {
		return assets.get(n);
	}
}
