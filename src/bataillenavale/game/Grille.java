package bataillenavale.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

/**
 * @author Jeremie
 *         Date: 27/03/14, 11:29
 */
public class Grille extends Observable {
	private int width, height;
	private Epoque epoque;
	private boolean[][] grilleTirs;
	private boolean[][] grillePortee;
	private GrilleNavire[][] grilleNavires;
	private List<GrilleNavire> navires;

	/**
	 * Construit une nouvelle grille avec les dimensions donnees.
	 *
	 * @param width  Largeur de la grille
	 * @param height Hauteur de la grille
	 */
	public Grille(int width, int height, Epoque epoque) {
		this.width = width;
		this.height = height;
		this.epoque = epoque;
		this.grilleTirs = new boolean[width][height];
		this.grillePortee = new boolean[width][height];
		this.grilleNavires = new GrilleNavire[width][height];
		navires = Collections.synchronizedList(new ArrayList<GrilleNavire>());
	}

	public Grille() {

	}

	/**
	 * Retourne le navire aux coordonnees donnees
	 *
	 * @param coord Les coordonnees sur la grille
	 * @return Le navire de la grille a cet emplacement. Note que .getPosition() renvoit la position de la premiere case du bateau et peut donc differer de l'argument coord.
	 * @throws java.lang.IllegalArgumentException si les coordonnees ne sont pas valide dans la grille
	 */
	public GrilleNavire getNavire(Coordonnees coord) {
		checkCoordonees(coord);
		return grilleNavires[coord.x][coord.y];
	}

	/**
	 * Realise un tir sur la grille
	 * Cette fonction est package-private pour s'assurer que seule EtatPartie l'appelle
	 *
	 * @param coord Coordonnees du tir
	 * @return True s'il y avait un navire a cet endroit. False sinon
	 * @throws java.lang.IllegalArgumentException si les coordonnees ne sont pas valide dans la grille ou s'il y a deja un tir a cet endroit
	 */
	public synchronized boolean tir(Coordonnees coord) {
		checkCoordonees(coord);
		if (grilleTirs[coord.x][coord.y])
			throw new IllegalArgumentException("Deja un tir a cet endroit");
		grilleTirs[coord.x][coord.y] = true;
		updatePortee();
		if (grilleNavires[coord.x][coord.y] != null)
			grilleNavires[coord.x][coord.y].touche();
		setChanged();
		notifyObservers(coord);
		return grilleNavires[coord.x][coord.y] != null;
	}

	/**
	 * Retourne si une case de la grille a recu un tir.
	 *
	 * @param coord Les coordonnees sur la grille
	 * @return True s'il y a eu un tir, false sinon
	 * @throws java.lang.IllegalArgumentException si les coordonnees ne sont pas valides dans la grille
	 */
	public boolean getTir(Coordonnees coord) {
		checkCoordonees(coord);
		return grilleTirs[coord.x][coord.y];
	}

	/**
	 * Retourne si le joueur peut attaquer une case donnee
	 *
	 * @param coord Corrdonnees de la case
	 * @return True si le joueur peut attaquer la grille de l'adversaire aux coordonnees donnees
	 * @throws java.lang.IllegalArgumentException si les coordonnees ne sont pas valides dans la grille
	 */
	public boolean canTir(Coordonnees coord) {
		checkCoordonees(coord);
		return grillePortee[coord.x][coord.y];
	}

	/**
	 * Retourne s'il est possible d'ajouter un navire a un emplacement donne (c'est a dire, toutes les cases du navire dans la grille et a un emplacement libre
	 *
	 * @param nav   Le type de navire
	 * @param coord Les coordonnees sur la grille, rotation prise en compte
	 * @return True s'il est possible d'ajouter ce navire, false sinon
	 * @throws java.lang.IllegalArgumentException si les coordonnees ne sont pas valides dans la grille ou si l'un des argument est null
	 */
	public synchronized boolean canAdd(Navire nav, Coordonnees coord) {
		if (nav == null || coord == null || !coordonneesValides(coord))
			throw new IllegalArgumentException(String.format("Argument null (%s, %s)", nav, coord));
		int cnt = 0;
		for (GrilleNavire navire : navires)
			if (navire.getNavire() == nav)
				cnt++;
		if (cnt >= nav.getNombreMax())
			return false;
		for (int i = 0; i < nav.getTaille(); i++) {
			int x = coord.x + (1 - coord.rotation) % 2 * i;
			int y = coord.y + (2 - coord.rotation) % 2 * i;
			//System.out.printf("Tested : %d:%d -> %b || %b || %b || %b || %b\n", x, y, x < 0, x >= width, y < 0, y >= height, grilleNavires[x][y] != null);
			if (x < 0 || x >= width || y < 0 || y >= height || grilleNavires[x][y] != null)
				return false;
		}
		return true;
	}

	/**
	 * Ajoute un navire a la grille a l'emplacement donne
	 * Toujours verifier la possibilité d'ajouter le bateau avant avec canAdd()
	 *
	 * @param nav   Le type de navire
	 * @param coord Les coordonnees sur la grille, rotation prise en compte
	 * @throws java.lang.IllegalArgumentException si l'un des argument est null, si les coordonnees ne sont pas valides dans la grille ou s'il n'est pas possible de placer ce navire a cet endroit
	 */
	public synchronized void addNavire(Navire nav, Coordonnees coord) {
		if (!canAdd(nav, coord))
			throw new IllegalArgumentException(String.format("Impossible d'ajouter le bateau (%s, %s)", nav.name(), coord));
		GrilleNavire navire = new GrilleNavire(nav, coord);
		navires.add(navire);
		for (int i = 0; i < nav.getTaille(); i++) {
			int x = coord.x + (1 - coord.rotation) % 2 * i;
			int y = coord.y + (2 - coord.rotation) % 2 * i;
			grilleNavires[x][y] = navire;
		}
		updatePortee();
		setChanged();
		notifyObservers(navire);
	}

	/**
	 * Retourne la liste des navires sur la grille. La liste reste synchronisee avec celle de la grille.
	 *
	 * @return La liste des navires associes a leur coordonnee (non modifiable)
	 */
	public List<GrilleNavire> getNavires() {
		return Collections.unmodifiableList(navires);
	}

	public boolean coordonneesValides(Coordonnees coord) {
		return (coord != null && coord.x >= 0 && coord.y >= 0 && coord.x < width && coord.y < height);
	}

	private synchronized void checkCoordonees(Coordonnees coord) { // Methode interne - throw une exception si les coordonnees sont invalide
		if (!coordonneesValides(coord))
			throw new IllegalArgumentException("Coordonnees invalides (" + coord + ")");
	}

	/**
	 * Retourne la largeur de la grille
	 *
	 * @return La taille de la grille sur sa premiere dimension
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Retourne la hauteur de la grille
	 *
	 * @return La taille de la grille sur sa seconde dimension
	 */
	public int getHeight() {
		return height;
	}

	public boolean[][] getGrilleTirs() {
		return grilleTirs;
	}

	public boolean[][] getGrillePortee() {
		return grillePortee;
	}

	public void setGrilleTirs(boolean[][] grilleTirs) {
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if (grilleTirs[i][j]) {
					tir(new Coordonnees(i, j));
				}
			}
		}
	}

	public void setNavires(List<GrilleNavire> navires) {
		for (GrilleNavire gn : navires) {
			addNavire(gn.getNavire(), gn.getPosition());
		}
	}

	/**
	 * Retourne le nombre de navires presents sur la grille
	 *
	 * @return Le nombre de navires sur la grille
	 */
	public int getNbNavires() {
		return navires.size();
	}

	private void updatePortee() {
		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
				grillePortee[i][j] = false;

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if (!grilleTirs[i][j] && grilleNavires[i][j] != null) {
					int portee = epoque.getFirepower(grilleNavires[i][j].getNavire());
					int xmin = Math.max(0, i - portee), ymin = Math.max(0, j - portee), xmax = Math.min(width - 1, i + portee), ymax = Math.min(height - 1, j + portee);
					for (int x = xmin; x <= xmax; x++)
						for (int y = ymin; y <= ymax; y++) {
							if (Math.hypot(x - i, y - j) <= portee - 0.1) // Decalage pour eviter les erreurs d'arrondi
								grillePortee[x][y] = true;
						}
				}
			}
		}
	}

	/**
	 * Affiche une representation textuelle de la grille.
	 * Les navires sont representes par la premiere lettre de leur type, en minuscule si la case s'est fait touche, majuscule sinon.
	 * Les cases vide sont represente par un ~, ou X s'il y a eu un tir a cet emplacement
	 */
	public synchronized void printContent(boolean hidden) {
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				GrilleNavire nav = grilleNavires[i][j];
				char c;
				if (nav != null && !hidden) {
					c = nav.getNavire().name().charAt(0);
					if (grilleTirs[i][j])
						c = Character.toLowerCase(c);
				} else if (nav != null)
					c = (grilleTirs[i][j]) ? 'X' : '~';
				else
					c = (grilleTirs[i][j]) ? 'O' : '~';
				System.out.print(c);
			}
			System.out.println();
		}
		System.out.println();
	}

	public void printContent() {
		printContent(false);
	}

	public synchronized void printPortee() {
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				System.out.print(grillePortee[i][j] ? 'X' : ' ');
			}
			System.out.println();
		}
		System.out.println();
	}

	/**
	 * Represente un navire sur la grille (Type, Emplacement, Vie)
	 */
	public class GrilleNavire {
		private Navire navire;
		private Coordonnees position;
		private int vie;

		public GrilleNavire(Navire navire, Coordonnees position) {
			this.navire = navire;
			this.position = position;
			vie = navire.getTaille();
		}

		private void touche() {
			if (vie == 0)
				throw new IllegalStateException(String.format("Tir sur navire coule (%s,%s)", navire, position));
			vie--;
		}

		/**
		 * Retourne le type de navire
		 *
		 * @return Le type de navire
		 */
		public Navire getNavire() {
			return navire;
		}

		/**
		 * Retourne la position du navire sur la grille
		 *
		 * @return La position du navire
		 */
		public Coordonnees getPosition() {
			return position;
		}

		/**
		 * Retourne la vie restante du navire, i.e le nombre de cases non touchees restantes
		 *
		 * @return La vie restante du navire
		 */
		public int getVie() {
			return vie;
		}

		/**
		 * Retourne si le navire a ete coule
		 *
		 * @return True si vie est a 0, false sinon
		 */
		public boolean isCoule() {
			return vie == 0;
		}

		@Override
		public String toString() {
			return "Navire " + navire + " en " + position;
		}
	}

}
