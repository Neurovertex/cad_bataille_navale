package bataillenavale.game;

import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * Cette classe représente les options de jeu configurable par le joueur. Elle est immuable pour eviter les effets de bords dans les classes l'utilisant.
 */
public class OptionsJeu {
	private final String strategieIA;
	private final int largeurGrille;
	private final int hauteurGrille;
	private final String epoque;
	private final String placement;
	private static final Preferences prefs = Preferences.userNodeForPackage(OptionsJeu.class);

	public OptionsJeu() {
		synchronized (prefs) {
			strategieIA = prefs.get("options.strategieia", "random");
			largeurGrille = prefs.getInt("options.largeurgrille", 10);
			hauteurGrille = prefs.getInt("options.hauteurgrille", 10);
			epoque = prefs.get("options.epoque", "moderne");
			placement = prefs.get("options.placement", "manual");
		}
	}

	public OptionsJeu(String strategieIA, int largeurGrille, int hauteurGrille, String epoque, String placement) {
		if (strategieIA == null || strategieIA.length() == 0 || largeurGrille < 5 || hauteurGrille < 5 ||
				epoque == null || epoque.length() == 0 || placement == null || placement.length() == 0)
			throw new IllegalArgumentException(String.format("Options de jeu invalide (%s, %d, %d, %s, %s)", strategieIA, largeurGrille, hauteurGrille, epoque, placement));
		this.strategieIA = strategieIA;
		this.largeurGrille = largeurGrille;
		this.hauteurGrille = hauteurGrille;
		this.epoque = epoque;
		this.placement = placement;
	}

	public void saveOptions() throws BackingStoreException {
		synchronized (prefs) {
			prefs.put("options.strategie", strategieIA);
			prefs.putInt("options.largeurgrille", largeurGrille);
			prefs.putInt("options.hauteurgrille", hauteurGrille);
			prefs.put("options.epoque", epoque);
			prefs.put("options.placement", placement);
			prefs.flush();
		}
	}

	public String getStrategieIA() {
		return strategieIA;
	}

	public int getLargeurGrille() {
		return largeurGrille;
	}

	public int getHauteurGrille() {
		return hauteurGrille;
	}

	public String getEpoque() {
		return epoque;
	}

	public String getPlacement() {
		return placement;
	}
}
