package bataillenavale.game;

import bataillenavale.game.epoque.EpoqueFactory;
import bataillenavale.game.ia.StrategieIAFactory;

import java.util.List;
import java.util.Observable;

public class EtatPartie extends Observable {
	public static final int TOTAL_NAVIRES_MAX;

	static {
		int total = 0;
		for (Navire nav : Navire.values())
			total += nav.getNombreMax();
		TOTAL_NAVIRES_MAX = total;
	}

	private Grille grilleJoueur;
	private Grille grilleIA;
	private OptionsJeu options;
	private StrategieIA ia;
	private final Epoque epoque;
	private Etat etat;

	public EtatPartie(OptionsJeu options) {
		this.options = options;
		epoque = EpoqueFactory.getEpoque(options.getEpoque());
		grilleJoueur = new Grille(options.getLargeurGrille(), options.getHauteurGrille(), epoque);
		grilleIA = new Grille(options.getLargeurGrille(), options.getHauteurGrille(), epoque);
		ia = StrategieIAFactory.getStrategie(options.getStrategieIA());
		placementAleatoire(grilleIA);
		if (options.getPlacement().equals("random")) {
			placementAleatoire(grilleJoueur);
			setEtat(Etat.ATTENTE);
		} else {
			setEtat(Etat.PLACEMENT);
		}
	}

	private static void placementAleatoire(Grille grille) {
		Coordonnees coord;
		for (Navire nav : Navire.values()) {
			for (int i = 0; i < nav.getNombreMax(); i++) {
				do {
					coord = new Coordonnees((int) (Math.random() * grille.getWidth()), (int) (Math.random() * grille.getHeight()), (int) (Math.random() * 4));
				} while (!grille.canAdd(nav, coord));
				grille.addNavire(nav, coord);
			}
		}
	}

	public void verifieFinJeu() {
		List<Grille.GrilleNavire> navireIA = grilleIA.getNavires(), navireJoueur = grilleJoueur.getNavires();
		int vivantIA = 0, vivantJoueur = 0;
		for (Grille.GrilleNavire nav : navireIA)
			if (!nav.isCoule()) {
				vivantIA++;
			}

		if (vivantIA == 0) {
			setEtat(Etat.VICTOIRE_JOUEUR);
			return;
		}
		for (Grille.GrilleNavire nav : navireJoueur)
			if (!nav.isCoule()) {
				vivantJoueur++;
			}
		if (vivantJoueur == 0) {
			setEtat(Etat.VICTOIRE_IA);
			return;
		}

		boolean canPlayIA = false, canPlayJoueur = false;
		Coordonnees coord;
		for (int j = 0; j < options.getHauteurGrille(); j++)
			for (int i = 0; i < options.getLargeurGrille(); i++) {
				coord = new Coordonnees(i, j);
				if (grilleIA.canTir(coord) && !grilleJoueur.getTir(coord))
					canPlayIA = true;
				if (grilleJoueur.canTir(coord) && !grilleIA.getTir(coord))
					canPlayJoueur = true;
				if (canPlayIA && canPlayJoueur)
					break;
			}
		if (!canPlayIA || !canPlayJoueur) {
			if (vivantIA > vivantJoueur)
				setEtat(Etat.VICTOIRE_IA);
			else if (vivantIA < vivantJoueur)
				setEtat(Etat.VICTOIRE_JOUEUR);
			else
				setEtat(Etat.MATCH_NUL);
		}
	}

	public void jouerIA() {
		setEtat(Etat.TOUR_IA);
		grilleJoueur.tir(ia.jouer(this));
		setEtat(Etat.ATTENTE);
		verifieFinJeu();
	}

	public void jouerJoueur() throws InterruptedException {
		setEtat(Etat.TOUR_JOUEUR);
		synchronized (this) {
			wait(); // On attend qu'un autre thread (interface grapghique/textuelle) joue
		}
		setEtat(Etat.ATTENTE);
		verifieFinJeu();
	}

	public boolean jouer(Coordonnees coord) {
		if (etat != Etat.TOUR_JOUEUR || !grilleIA.coordonneesValides(coord) || grilleIA.getTir(coord) || !grilleJoueur.canTir(coord))
			return false;
		grilleIA.tir(coord);
		synchronized (this) {
			notifyAll();
		}
		return true;
	}

	public boolean placer(Coordonnees coord, Navire nav) {
		if (etat != Etat.PLACEMENT || !grilleJoueur.coordonneesValides(coord) || !grilleJoueur.canAdd(nav, coord))
			return false;
		grilleJoueur.addNavire(nav, coord);
		if (grilleJoueur.getNavires().size() == TOTAL_NAVIRES_MAX)
			setEtat(Etat.ATTENTE);
		return true;
	}

	public boolean isFini() {
		return etat == Etat.VICTOIRE_IA || etat == Etat.VICTOIRE_JOUEUR || etat == Etat.MATCH_NUL || etat == Etat.INTERROMPU;
	}

	public void stop() {
		setEtat(Etat.INTERROMPU);
	}

	public Grille getGrilleJoueur() {
		return grilleJoueur;
	}

	public Grille getGrilleIA() {
		return grilleIA;
	}

	public OptionsJeu getOptions() {
		return options;
	}

	public Epoque getEpoque() {
		return epoque;
	}

	public StrategieIA getIa() {
		return ia;
	}

	public void setOptions(OptionsJeu options) {
		if (!options.getEpoque().equals(this.options.getEpoque()) ||
				options.getHauteurGrille() != this.options.getHauteurGrille() ||
				options.getLargeurGrille() != this.options.getLargeurGrille() ||
				!options.getPlacement().equals(this.options.getPlacement()))
			throw new IllegalArgumentException("Illegal option change");
		ia = StrategieIAFactory.getStrategie(options.getStrategieIA());
		this.options = options;
	}

	public void setEtat(Etat etat) {
		this.etat = etat;
		setChanged();
		notifyObservers(etat);
	}

	public void setGrilleJoueur(Grille g) {
		this.grilleJoueur = g;
	}

	public void setGrilleIA(Grille grilleIA) {
		this.grilleIA = grilleIA;
	}

	public Etat getEtat() {
		return etat;
	}

	public enum Etat {
		PLACEMENT, ATTENTE, TOUR_JOUEUR, TOUR_IA, VICTOIRE_JOUEUR, VICTOIRE_IA, MATCH_NUL, INTERROMPU
	}
}
