package bataillenavale.game;

public interface StrategieIA {
	public abstract Coordonnees jouer(EtatPartie etat);

	public abstract String getNom();
}
