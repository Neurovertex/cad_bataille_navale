package bataillenavale.game;

import java.util.Observable;
import java.util.Observer;

/**
 * @author Jeremie
 *         Date: 08/04/14, 17:51
 */
public class BoucleJeu extends Thread implements Observer {
	private final EtatPartie etat;

	public BoucleJeu(EtatPartie etat) {
		this.etat = etat;
		etat.addObserver(this);
	}

	public BoucleJeu(OptionsJeu options) {
		this(new EtatPartie(options));
	}

	@Override
	public void run() {
		try {
			boolean ia = false;
			while (!etat.isFini()) {
				if (etat.getEtat() != EtatPartie.Etat.ATTENTE && etat.getEtat() != EtatPartie.Etat.TOUR_JOUEUR)
					synchronized (this) {
						wait();
						continue;
					}
				if (!ia)
					etat.jouerJoueur();
				else
					etat.jouerIA();
				ia = !ia;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		if (o == etat && etat.getEtat() == EtatPartie.Etat.ATTENTE || etat.isFini())
			synchronized (this) {
				notifyAll();
			}
	}
}
