package bataillenavale.game;

/**
 * @author Jeremie
 *         Date: 27/03/14, 12:54
 */
public enum Navire {
	TINY(2, 2), SMALL(3, 2), MEDIUM(3, 1), LARGE(4, 1), HUGE(5, 1);

	private int taille, nombreMax;
	private Navire(int taille, int nombreMax) {
		this.taille = taille;
		this.nombreMax = nombreMax;
	}
	public int getTaille() {
		return taille;
	}

	public int getNombreMax() {
		return nombreMax;
	}
}
