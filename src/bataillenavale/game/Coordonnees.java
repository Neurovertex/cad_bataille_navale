package bataillenavale.game;

/**
 * Classe representant des coordonnees dans la grille, avec optionellement une rotation (e.g pour le placement des navires).
 * Etant donne son immuabilite ses attributs sont public pour alleger le code. La rotation est décrite comme un entier dans 0-3, pour respectivement droite, bas, gauche, haut.
 * @author Jeremie
 *         Date: 27/03/14, 11:28
 */
public class Coordonnees {
	public final int x, y, rotation;

	public Coordonnees(int x, int y, int rotation) {
		this.x = x;
		this.y = y;
		this.rotation = rotation%4;
	}

	public Coordonnees(int x, int y) {
		this(x, y, 0);
	}

	@Override
	public boolean equals(Object o) {
		if (o == null || ! (o instanceof Coordonnees))
			return false;
		Coordonnees c = (Coordonnees)o;
		return c.x == x && c.y == y && c.rotation == rotation;
	}

	@Override
	public String toString() {
		char[] arrows = {'→', '↓', '←', '↑'};
		return String.format("<%d,%d,%c>", x, y, arrows[rotation]);
	}
}
